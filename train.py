import asyncio
import pandas as pd
from models import CryptoMoney, BinanceRequest
from sklearn.linear_model import LinearRegression, PassiveAggressiveRegressor
import time

#вот тут расписать правильно получение модели
# оставить возможность обучиться на обычной линейной регрессии 
# использовать model PassiveAggressiveRegressor и partial_fit из sklearn
async def get_model(dependence_cc : CryptoMoney ):
    '''
    создаем и обучаем модельку 
    '''
    params = {
        'symbol' : CryptoMoney.symbol,
        'limit' : 1000,
        'interval' : '1s'       
    }
    data_BTC, data_ETH = await asyncio.gather(BinanceRequest.getKlines(money = btc), BinanceRequest.getKlines(money = eth))

    data = await BinanceRequest.getKlines(METHOD_URL='uiKlines', params=params)
    df = pd.DataFrame(data, params=params)
    return df

async def main():
    start_timer = time.time() 
    btc = CryptoMoney(short_name='BTC')
    eth = CryptoMoney(short_name='ETH')
    df_BTC, df_ETH = await asyncio.gather(BinanceRequest.getKlines(money = btc), BinanceRequest.getKlines(money = eth))
    results = df_ETH.merge(df_BTC, on=['Open_time'])

    X = results['Open_price_y'].values.reshape(-1, 1)
    open_price_eth_list = results['Open_price_x'].values.tolist()
    import statsmodels.api as sm
    dme = sm.tsa.acf(open_price_eth_list)
    print(dme)
    y = results['Open_price_x']
    model = LinearRegression()
    # X = results['Open_price_y'].astype(float).values.reshape(-1, 1)
    # y = results['Open_price_x'].astype(float)
    model.fit(X, y)
    # print(result_model.summary())
    results['predicted_ETHUSDT'] = model.predict(X)
    # print(results['predicted_ETHUSDT'])
    results['residuals'] = results['Open_price_x'].astype(float) - results['predicted_ETHUSDT']

    finish_time = time.time()
    work_time = finish_time - start_timer
    print(work_time)
    print(results[['Open_price_x', 'Open_price_y', 'residuals']])
    print(df_BTC.head())
    print(df_ETH.head())
    results.to_excel('test2.xlsx')

if __name__ == "__main__":
    asyncio.run(main())