FROM python:3.11-slim
WORKDIR /app
COPY requirements.txt ./
COPY config.json ./
RUN pip --default-timeout=60 install -r requirements.txt
COPY . .
CMD [ "python", "./main.py" ]
