package main

import (
	"encoding/json"

	"github.com/go-resty/resty/v2"
)

// TODO
// сделать подсчет 5 минутной разницы
// сделать api для получения данных из нескольких источников:
// 1. coingecko
// 2. coinmarketcap +
// 3. cryptocompare
// 4. nomics
// func fetchCoinPrices() ([]Coin, error) {
// 	client := resty.New()
// 	resp, err := client.R().Get("https://api.coingecko.com/api/v3/coins/markets?vs_currency=usdt")

// 	if err != nil {
// 		log.Fatalf("Error fetching data: %v", err)
// 		return nil, err
// 	}
// 	var coins []Coin
// 	err = json.Unmarshal(resp.Body(), &coins)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return coins, nil
// }

type CMCResponse struct {
	Status Status `json:"status"`
	Data   []Data `json:"data"`
}

type Status struct {
	Timestamp    string  `json:"timestamp"`
	ErrorCode    int     `json:"error_code"`
	ErrorMessage *string `json:"error_message"`
	Elapsed      int     `json:"elapsed"`
	CreditCount  int     `json:"credit_count"`
}

type Data struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Symbol string `json:"symbol"`
	Slug   string `json:"slug"`
	Quote  Quote  `json:"quote"`
}

type Quote struct {
	USD Currency `json:"USD"`
}

type Currency struct {
	Price            float64 `json:"price"`
	Volume24h        float64 `json:"volume_24h"`
	MarketCap        float64 `json:"market_cap"`
	PercentChange1h  float64 `json:"percent_change_1h"`
	PercentChange24h float64 `json:"percent_change_24h"`
	PercentChange7d  float64 `json:"percent_change_7d"`
	LastUpdated      string  `json:"last_updated"`
}

func fetchCoinPrices() (*CMCResponse, error) {
	url := "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
	apiKey := "7c1a3096-1d3a-4e5f-bb48-d4cb62fda73e"

	client := resty.New()
	resp, err := client.R().
		SetHeader("X-CMC_PRO_API_KEY", apiKey).
		Get(url)

	if err != nil {
		return nil, err
	}

	var response CMCResponse
	err = json.Unmarshal([]byte(resp.Body()), &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}
