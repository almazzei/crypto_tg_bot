#!/usr/bin/env python
# pylint: disable=unused-argument, wrong-import-position
# This program is dedicated to the public domain under the CC0 license.

import logging
import asyncio
from telegram import ForceReply, Update, Bot, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    Application, MessageHandler,
    ContextTypes,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    InvalidCallbackData,
    filters,
    Updater,
)

from typing import List
from models import CryptoMoney
import json

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:

    cc_list = [
        {
            "name" : "Bitcoin",
            "short_name" : "BTC"
            },
        {
            "name" : "Etherium",
            "short_name" : "ETH"
        },
        {
            "name" : "Ripple",
            "short_name" : "XRP"
        }
    ]

    await update.message.reply_text("Выберите нужную криптовалюту", reply_markup=build_keyboard(cc_list))

async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    await update.message.reply_text("Help!")

def build_keyboard(cc_list: List[dict]) -> InlineKeyboardMarkup:
    """Helper function to build the next inline keyboard."""
    return InlineKeyboardMarkup.from_column(
        [InlineKeyboardButton(cc['name'], callback_data=cc['short_name']) for cc in cc_list]
    )

async def choice_CryptoMoney(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    # query = update.callback_query
    # await query.answer()
    cc_list = [
        {
            "name" : "Bitcoin",
            "short_name" : "BTC"
            },
        {
            "name" : "Etherium",
            "short_name" : "ETH"
        },
        {
            "name" : "Ripple",
            "short_name" : "XRP"
        }
    ]

    await update.message.reply_text("Выберите нужную криптовалюту", reply_markup=build_keyboard(cc_list))

async def create_graph(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()
    short_name = query.data if query.data else 'BTC'
    cc = CryptoMoney(short_name=short_name)
    converter_currency = 'USDT'

    # we can delete the data stored for the query, because we've replaced the buttons
    # вот тут ВАЖНАЯ штука - разберись 
    # context.drop_callback_data(query)

async def send_info(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()
    short_name = query.data
    cc = CryptoMoney(short_name=short_name)
    converter_currency = 'USDT'
    await query.edit_message_text("Запрос в обработке")
    price = await cc.getPrice(converter_currency = converter_currency)
    graph = await cc.getGraph(symbol = converter_currency, interval = '1m', limit = '1000')
    
    await query.edit_message_text(f"Текущая цена {cc.short_name} в {converter_currency}: {price}")
    # await query.message.reply_text(f"Текущая цена {cc.short_name} в {converter_currency}: {price}")
    await query.message.reply_photo(graph, caption=f'График Время/Цена {cc.short_name}')
    # await update.message.reply_document(graph, )

async def handle_invalid_button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Informs the user that the button is no longer available."""
    await update.callback_query.answer()
    await update.effective_message.edit_text(
        "Sorry, I could not process this button click 😕 Please send /start to get a new keyboard."
    )

def main() -> None:
    """Start the bot."""
    with open('config.json') as config_file:
        config = json.load(config_file)
        API_TOKEN = config['API_TOKEN']
    application = Application.builder().token(API_TOKEN).build()
    bot = Bot(API_TOKEN)
    update_queue = asyncio.Queue()
    updater = Updater(bot=bot, update_queue=update_queue)

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("change", choice_CryptoMoney))
    application.add_handler(CommandHandler("help", help_command))
    application.add_handler(
        CallbackQueryHandler(handle_invalid_button, pattern=InvalidCallbackData)
    )
    application.add_handler(CallbackQueryHandler(send_info))

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    application.run_polling(1.0)

if __name__ == "__main__":
    main()