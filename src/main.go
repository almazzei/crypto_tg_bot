package main

import (
	"log"
)

func main() {
	db, err := initDB()
	if err != nil {
		log.Fatalf("Ошибка подключения к базе данных: %v", err)
	}

	responce, err := fetchCoinPrices()
	if err != nil {
		log.Fatalf("Ошибка получения данных: %v", err)
	}

	err = saveCoins(db, *responce)
	if err != nil {
		log.Fatalf("Ошибка сохранения в базу данных: %v", err)
	}
}
