package main

import (
	"time"
)

func convertToCoins(response CMCResponse) []Coin {
	coins := make([]Coin, len(response.Data))

	for i, data := range response.Data {
		timestamp, _ := time.Parse(time.RFC3339, data.Quote.USD.LastUpdated)

		coin := Coin{
			CMCID:  data.ID,
			Name:   data.Name,
			Symbol: data.Symbol,
			Slug:   data.Slug,
			Quotes: []CoinQuote{
				{
					Price:            data.Quote.USD.Price,
					Volume24h:        data.Quote.USD.Volume24h,
					MarketCap:        data.Quote.USD.MarketCap,
					PercentChange1h:  data.Quote.USD.PercentChange1h,
					PercentChange24h: data.Quote.USD.PercentChange24h,
					PercentChange7d:  data.Quote.USD.PercentChange7d,
					Timestamp:        timestamp,
					Currency:         "USD",
				},
			},
		}
		coins[i] = coin
	}

	return coins
}
