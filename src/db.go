package main

import (
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Coin struct {
	ID        uint `gorm:"primaryKey"`
	CMCID     int  `gorm:"unique;column:cmc_id"`
	Name      string
	Symbol    string
	Slug      string
	CreatedAt time.Time
	UpdatedAt time.Time
	Quotes    []CoinQuote `gorm:"foreignKey:CoinID"`
}

type CoinQuote struct {
	ID               uint `gorm:"primaryKey"`
	CoinID           uint
	Price            float64
	Volume24h        float64
	MarketCap        float64
	PercentChange1h  float64
	PercentChange24h float64
	PercentChange7d  float64
	Timestamp        time.Time
	Currency         string `gorm:"default:USD"`
	CreatedAt        time.Time
}

func initDB() (*gorm.DB, error) {
	dsn := "host=localhost user=almaz password=burunduk dbname=crypto_bot port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// Автоматическая миграция схемы
	err = db.AutoMigrate(&Coin{}, &CoinQuote{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func saveCoins(db *gorm.DB, responce CMCResponse) error {
	coins := convertToCoins(responce)
	return db.Transaction(func(tx *gorm.DB) error {
		for _, coin := range coins {
			// Обновляем запись при конфликте cmc_id
			if err := tx.Clauses(clause.OnConflict{
				Columns:   []clause.Column{{Name: "cmc_id"}},
				DoUpdates: clause.AssignmentColumns([]string{"name", "symbol", "slug", "updated_at"}),
			}).Create(&coin).Error; err != nil {
				return err
			}
		}
		return nil
	})
}
