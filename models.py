# в данном файле описаны созданные классы и сущности
from datetime import datetime
import asyncio
import aiohttp
import io
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


class CryptoMoney:

    def __init__(self, short_name : str, converter_currency : str = 'USDT'):
        self.short_name = short_name
        self.converter_currency = converter_currency


    def symbol(self):
        return self.short_name + self.converter_currency

    def getPrice(self):
        return BinanceRequest.getPrice(self)

class BinanceRequest:
    '''
    класс запросов к api Binance 
    '''
    API_URL: str = 'https://api.binance.com/api/v3/'
    # TODO
    # также создать отдельный класс CryptoGraph для получения графика (остальные графики построить на основе него)

    @classmethod
    async def getKlines(cls, money : CryptoMoney, METHOD_URL : str = 'uiKlines', interval: str = '1m', limit: int = 1000, ):
        header = ['Open_time', 'Open_price', 'High_price', 'Low_price', 'Close_price', 'Close_time', 'Volume', 'Quote_asset_volume', 'Number_of_trades', 'Taker_buy_base_asset_volume', 'Taker_buy_quote_asset_volume', 'Unused_field_Ignore']

        params = {
            'symbol': money.short_name + money.converter_currency,
            'interval': interval,
            'limit' : limit,
            }
        async with aiohttp.ClientSession() as session:
            async with session.get(cls.API_URL + METHOD_URL, params=params) as response:
                if response.status == 200:
                    data = await response.json()
                    return pd.DataFrame(data, columns=header)
                else:
                    print('Error:', response.status)
                    return 0

    @classmethod
    async def getPrice(cls, money : CryptoMoney, METHOD_URL : str = 'ticker/price'):

        params = {
            'symbol': money.short_name + money.converter_currency
            }
        async with aiohttp.ClientSession() as session:
            async with session.get(cls.API_URL + METHOD_URL, params=params) as response:
                if response.status == 200:
                    data = await response.json()
                    return round(float(data['price']), 2)
                else:
                    print('Error:', response.status)
                    return 0


class CryptoGraph:
    async def getGraph(self, money, interval, limit):
        buf = io.BytesIO()

        df = await BinanceRequest.getKlines(money, interval=interval, limit=limit)
        binance_df = df.drop(columns=['Volume', 'Quote_asset_volume', 'Number_of_trades', 'Taker_buy_base_asset_volume', 'Taker_buy_quote_asset_volume', 'Unused_field_Ignore'], axis = 1)
        binance_df['Open_time'] = pd.to_datetime(binance_df['Open_time'], unit='ms')

        # binance_df['Close_time'] = pd.to_datetime(binance_df['Close_time'], unit='ms')
        binance_df = binance_df.astype({"Close_price": float, "Open_price": float, "Low_price": float, "High_price": float })
        sns.set_style("darkgrid") 
        sns.set(rc={"figure.figsize":(10, 5.5)})
        graph = sns.lineplot(x='Open_time', y='Open_price', data=binance_df, markersize=10, legend="auto")
        graph.set(xlabel='Время',
                ylabel='Цена',
                title=f'График Время/Цена {self.short_name}')
        # plt.yticks()
        # plt.savefig('plot.png')
        plt.savefig(buf)
        plt.close()
        buf.seek(0)
        image_data = buf.read()
        buf.close()
        return image_data

    

# Пример работы
if __name__ == "__main__":
    btc = CryptoMoney(short_name='BTC')
    data = asyncio.run(BinanceRequest.getPrice(money = btc))
    print('BITCOIN ДАННЫЕ')
    print(data)
    data = asyncio.run(BinanceRequest.getKlines(money = btc, limit=10))
    print(data)
    btc = CryptoMoney(short_name='ETH')
    data = asyncio.run(BinanceRequest.getPrice(money = btc))
    print('EHTERIUM ДАННЫЕ')
    print(data)
    data = asyncio.run(BinanceRequest.getKlines(money = btc, limit=10))
    print(data)
